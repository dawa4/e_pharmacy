const express=require("express")
const path=require('path')
const app=express()
const userRouter =require('./routes/userRoutes')
const viewsRouter=require('./routes/viewRouter')
module.exports=app

app.use(express.json())
app.use('/api/v1/users',userRouter)
app.use('/', viewsRouter)

app.use(express.static(path.join(__dirname,'views')))
// Strating the server on port 4001
// module.exports=app
const port=4001
app.listen(port,()=>{
    console.log(`App running on port ${port} ..`)
})